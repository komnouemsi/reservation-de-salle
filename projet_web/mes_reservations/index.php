
<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>Mes reservations</title>

		<!--Jquery et Angular-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.5/angular.min.js"></script>
		<!--<script src="../angular-1.4.8/angular.min.js"></script>-->

		<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="../formulaire/source/css/bootstrap.min.css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

	<!-- feuille de style -->
	<link type="text/css" rel="stylesheet" href="../formulaire/source_nav_bar/CSS.css" />
	<link type="text/css" rel="stylesheet" href="../formulaire/source/css/style_formulaire.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

	</head>
	<body ng-app="myApp">
		<div id="booking" class="section">
			<div id="wrapper" ng-controller="selectController">
				<!-- Sidebar -->   
				<div id="sidebar-wrapper">
				<ul class="sidebar-nav">
					<li><a href="../formulaire/example/index.php">Enregistrer une reservation</a></li>
					<li><a href="#">Déconnexion</a></li>
				</ul>
				</div>


				<div class="section-center" id="page-content-wrapper">
					<div class="container-fluid">
						<div class="row">
							<div class="col-lg-12">
								<button class="btn" id="menu-toggle"><span class="glyphicon glyphicon-menu-hamburger">g</span></button>
							</div>
							<div class="booking-form" >
								<div class="form-header">
									<h1>Réservations</h1>
								</div>
								<ul class="list-group list-group-flush">
									<li class="list-group-item" ng-repeat="x in reservation|filter:search:strict track by $index">
										&nbsp;&nbsp;{{ x.nom + ', ' + x.prenom}}
									</li>
									<li class="animate-repeat" ng-if="results.length === 0">
										<strong><i class="fa fa-exclamation-triangle fa-4x"></i>Aucun resultat trouvé...</strong>
									</li>
								</ul>
							</div>	
						</div>
					</div>
				</div>
			</div>	
		</div>
<script src="../formulaire/source/js/jquery.min.js"></script>

<!--fonction navbar-->
<script>
	$(document).ready(function(){
  $("#menu-toggle").click(function(e){
    e.preventDefault();
    $("#wrapper").toggleClass("menuDisplayed");
  });
});

</script>
<script>
var app = angular.module('myApp', []);
app.controller("selectController", function($scope, $http) {
    $http.get("getData.php") 
    .then(function(response) { 
    $scope.reservation = response.data; 
	console.log($scope.reservation);     
    });
 });
</script>
	</body>
</html>
