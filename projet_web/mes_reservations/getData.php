<?php
session_start();
$dbconn = pg_connect("host=localhost port=5432 dbname=bd_reservation user=postgres password=root");
    
// Check connection
if (!$dbconn) {
 die("Connection failed: " . pg_connect_error());
} 

if(isset($_SESSION["ine"])) {
    $id=$_SESSION["ine"];

    $result = pg_query($dbconn, "SELECT * FROM reservation where fk_etudiant='$id' and statut_reservation='En cours' order by id_reservation desc");

        if (!$result) {
        echo "Une erreur s'est produite au niveau de la requete de selection des reservations.\n";
        exit;
        }

        $data = array();

        while ($row = pg_fetch_array($result)) {

            $result1 = pg_query($dbconn, "SELECT * FROM creneau where num_creneau=".$row['fk_creneau']);
            $row1 = pg_fetch_array($result1);

            $result2 = pg_query($dbconn, "SELECT * FROM salle where nom_salle='$row[fk_salle]'");
            $row2 = pg_fetch_array($result2);

            $result3 = pg_query($dbconn, "SELECT * FROM etudiant where ine='$id'");
            $row3 = pg_fetch_array($result3);

            $data[] = array("date"=>$row['date_reservee'],"debut"=>$row1['heure_debut'],"fin"=>$row1['heure_fin'],"salle"=>$row2['nom_salle'],"nom"=>$row3['nom'],"prenom"=>$row3['prenom']);
        }
        echo json_encode($data);

  } else {
    echo "Les informations de l'utilisateur ne sont pas définies dans le cookie<br>";
    echo "Merci de recommencer l'opération";
  }

?>