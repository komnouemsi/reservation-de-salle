PGDMP     	                	    x            bd_reservation    12.1    12.1 $    .           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            /           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            0           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            1           1262    16393    bd_reservation    DATABASE     �   CREATE DATABASE bd_reservation WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'French_France.1252' LC_CTYPE = 'French_France.1252';
    DROP DATABASE bd_reservation;
                postgres    false            �            1259    16401    creneau    TABLE     �   CREATE TABLE public.creneau (
    num_creneau integer NOT NULL,
    heure_debut character varying NOT NULL,
    heure_fin character varying NOT NULL
);
    DROP TABLE public.creneau;
       public         heap    postgres    false            �            1259    16399    creneau_num_creneau_seq    SEQUENCE     �   CREATE SEQUENCE public.creneau_num_creneau_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.creneau_num_creneau_seq;
       public          postgres    false    204            2           0    0    creneau_num_creneau_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.creneau_num_creneau_seq OWNED BY public.creneau.num_creneau;
          public          postgres    false    203            �            1259    16425    etudiant    TABLE       CREATE TABLE public.etudiant (
    ine character varying(30) NOT NULL,
    nom character varying(30),
    prenom character varying(30),
    classe character varying(30),
    login character varying(30),
    mot_de_passe character varying(30),
    email character varying(30)
);
    DROP TABLE public.etudiant;
       public         heap    postgres    false            �            1259    16409    planification    TABLE     �   CREATE TABLE public.planification (
    id_planification integer NOT NULL,
    fk_salle character varying(30),
    fk_creneau integer,
    jour date
);
 !   DROP TABLE public.planification;
       public         heap    postgres    false            �            1259    16407 $   plannification_id_plannification_seq    SEQUENCE     �   CREATE SEQUENCE public.plannification_id_plannification_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public.plannification_id_plannification_seq;
       public          postgres    false    206            3           0    0 $   plannification_id_plannification_seq    SEQUENCE OWNED BY     k   ALTER SEQUENCE public.plannification_id_plannification_seq OWNED BY public.planification.id_planification;
          public          postgres    false    205            �            1259    16432    reservation    TABLE     /  CREATE TABLE public.reservation (
    id_reservation integer NOT NULL,
    date_reservation date,
    heure time without time zone,
    statut_reservation character varying(30),
    fk_salle character varying(30),
    fk_etudiant character varying(30),
    fk_creneau integer,
    date_reservee date
);
    DROP TABLE public.reservation;
       public         heap    postgres    false            �            1259    16430    reservation_id_reservation_seq    SEQUENCE     �   CREATE SEQUENCE public.reservation_id_reservation_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.reservation_id_reservation_seq;
       public          postgres    false    209            4           0    0    reservation_id_reservation_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.reservation_id_reservation_seq OWNED BY public.reservation.id_reservation;
          public          postgres    false    208            �            1259    16394    salle    TABLE     k   CREATE TABLE public.salle (
    nom_salle character varying(30) NOT NULL,
    nbre_places_salle integer
);
    DROP TABLE public.salle;
       public         heap    postgres    false            �
           2604    16404    creneau num_creneau    DEFAULT     z   ALTER TABLE ONLY public.creneau ALTER COLUMN num_creneau SET DEFAULT nextval('public.creneau_num_creneau_seq'::regclass);
 B   ALTER TABLE public.creneau ALTER COLUMN num_creneau DROP DEFAULT;
       public          postgres    false    204    203    204            �
           2604    16412    planification id_planification    DEFAULT     �   ALTER TABLE ONLY public.planification ALTER COLUMN id_planification SET DEFAULT nextval('public.plannification_id_plannification_seq'::regclass);
 M   ALTER TABLE public.planification ALTER COLUMN id_planification DROP DEFAULT;
       public          postgres    false    206    205    206            �
           2604    16435    reservation id_reservation    DEFAULT     �   ALTER TABLE ONLY public.reservation ALTER COLUMN id_reservation SET DEFAULT nextval('public.reservation_id_reservation_seq'::regclass);
 I   ALTER TABLE public.reservation ALTER COLUMN id_reservation DROP DEFAULT;
       public          postgres    false    208    209    209            &          0    16401    creneau 
   TABLE DATA           F   COPY public.creneau (num_creneau, heure_debut, heure_fin) FROM stdin;
    public          postgres    false    204   ,       )          0    16425    etudiant 
   TABLE DATA           X   COPY public.etudiant (ine, nom, prenom, classe, login, mot_de_passe, email) FROM stdin;
    public          postgres    false    207   U,       (          0    16409    planification 
   TABLE DATA           U   COPY public.planification (id_planification, fk_salle, fk_creneau, jour) FROM stdin;
    public          postgres    false    206   �,       +          0    16432    reservation 
   TABLE DATA           �   COPY public.reservation (id_reservation, date_reservation, heure, statut_reservation, fk_salle, fk_etudiant, fk_creneau, date_reservee) FROM stdin;
    public          postgres    false    209    -       $          0    16394    salle 
   TABLE DATA           =   COPY public.salle (nom_salle, nbre_places_salle) FROM stdin;
    public          postgres    false    202   �-       5           0    0    creneau_num_creneau_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.creneau_num_creneau_seq', 12, true);
          public          postgres    false    203            6           0    0 $   plannification_id_plannification_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public.plannification_id_plannification_seq', 13, true);
          public          postgres    false    205            7           0    0    reservation_id_reservation_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.reservation_id_reservation_seq', 4, true);
          public          postgres    false    208            �
           2606    16406    creneau creneau_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.creneau
    ADD CONSTRAINT creneau_pkey PRIMARY KEY (num_creneau);
 >   ALTER TABLE ONLY public.creneau DROP CONSTRAINT creneau_pkey;
       public            postgres    false    204            �
           2606    16429    etudiant etudiant_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.etudiant
    ADD CONSTRAINT etudiant_pkey PRIMARY KEY (ine);
 @   ALTER TABLE ONLY public.etudiant DROP CONSTRAINT etudiant_pkey;
       public            postgres    false    207            �
           2606    16414 !   planification plannification_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.planification
    ADD CONSTRAINT plannification_pkey PRIMARY KEY (id_planification);
 K   ALTER TABLE ONLY public.planification DROP CONSTRAINT plannification_pkey;
       public            postgres    false    206            �
           2606    16437    reservation reservation_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT reservation_pkey PRIMARY KEY (id_reservation);
 F   ALTER TABLE ONLY public.reservation DROP CONSTRAINT reservation_pkey;
       public            postgres    false    209            �
           2606    16398    salle salle_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.salle
    ADD CONSTRAINT salle_pkey PRIMARY KEY (nom_salle);
 :   ALTER TABLE ONLY public.salle DROP CONSTRAINT salle_pkey;
       public            postgres    false    202            �
           2606    16420 ,   planification plannification_fk_creneau_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.planification
    ADD CONSTRAINT plannification_fk_creneau_fkey FOREIGN KEY (fk_creneau) REFERENCES public.creneau(num_creneau);
 V   ALTER TABLE ONLY public.planification DROP CONSTRAINT plannification_fk_creneau_fkey;
       public          postgres    false    204    2714    206            �
           2606    16415 *   planification plannification_fk_salle_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.planification
    ADD CONSTRAINT plannification_fk_salle_fkey FOREIGN KEY (fk_salle) REFERENCES public.salle(nom_salle);
 T   ALTER TABLE ONLY public.planification DROP CONSTRAINT plannification_fk_salle_fkey;
       public          postgres    false    202    2712    206            �
           2606    16438 '   reservation reservation_fk_creneau_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT reservation_fk_creneau_fkey FOREIGN KEY (fk_creneau) REFERENCES public.creneau(num_creneau);
 Q   ALTER TABLE ONLY public.reservation DROP CONSTRAINT reservation_fk_creneau_fkey;
       public          postgres    false    209    2714    204            �
           2606    16448 (   reservation reservation_fk_etudiant_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT reservation_fk_etudiant_fkey FOREIGN KEY (fk_etudiant) REFERENCES public.etudiant(ine);
 R   ALTER TABLE ONLY public.reservation DROP CONSTRAINT reservation_fk_etudiant_fkey;
       public          postgres    false    207    2718    209            �
           2606    16443 %   reservation reservation_fk_salle_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT reservation_fk_salle_fkey FOREIGN KEY (fk_salle) REFERENCES public.salle(nom_salle);
 O   ALTER TABLE ONLY public.reservation DROP CONSTRAINT reservation_fk_salle_fkey;
       public          postgres    false    2712    209    202            &   ?   x���� �3��X�A���s(��s`!V���"�9�f�~��rQB�FRz����Cr�l      )   Q   x�342615515	v	vv����ļT(��eRffnai�ș�_\�ș������ia ��3�s3s���s�b���� 1�"      (   Z   x�m���0Dѳ��^{I�u���r}�X�H���`��I����1x�?ֶ�X�j,�x9�����n�ah�{�_��C�}�h͔*!      +   P   x��˱�0D���%��v�.0A��
�/�31��
�: ��K���{��W�R��Ő5~3{����	.      $   +   x�34��46�240�P&�B�A(se�� J� Jb���� d�
     