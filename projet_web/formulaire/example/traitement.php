<?php

//Cette fonction vérifie le token
//Vous passez en argument le temps de validité (en secondes)
//Le referer attendu (adresse absolue, rappelez-vous :D)
//Le nom optionnel si vous en avez défini un lors de la création du token
function verifier_token($temps, $referer, $nom = '')
{
session_start();
if(isset($_SESSION[$nom.'_token']) && isset($_SESSION[$nom.'_token_time']) && isset($_POST['token'])){
    if($_SESSION[$nom.'_token'] == $_POST['token']){
        if($_SESSION[$nom.'_token_time'] >= (time() - $temps)){ 
            if($_SERVER['HTTP_REFERER'] == $referer){ 
                return true;
            }			
        }		
    }	
}
	else {
        return false;
    }

}

if(verifier_token(600, 'http://localhost/Projet_web/formulaire/example/', 'enregistrement'))
    {
        $host = "localhost"; /* Host name */
        $user = "postgres"; /* User */
        $password = "root"; /* Password */
        $dbname = "bd_reservation"; /* Database name */
        $port="5432";
        
        try{
            $con="pgsql:host=" . $host. ";port=" .$port. ";dbname=" .$dbname. ";user=" .$user. ";password=" .$password;
        
            $pdo = new PDO($con,$user,$password);
            $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_OBJ);
            $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES,false);
            $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        }
        
        catch (PDOException $e){
            echo 'connection failed' .$e->getMessage();
        }
    

        $date_reservee = date('Y-m-d',strtotime($_POST['date_reservee']));
        $salle = $_POST['salle'];
        $ine = $_POST['ine'];
        $email = $_POST['email'];
        $today = date('Y-m-d');
        $heure = date("H:i");

        if (isset($_POST['creneau1'])){
            $creneau1 = $_POST['creneau1'];
            $requet="INSERT INTO reservation(date_reservation, heure, date_reservee, fk_salle, fk_etudiant, fk_creneau) VALUES('$today', '$heure','$date_reservee', '$salle', '$ine', '$creneau1')";
        $pdo->exec($requet);
        echo "success1";
        }
        if (isset($_POST['creneau2'])){
            $creneau2 = $_POST['creneau2'];
            $requet="INSERT INTO reservation(date_reservation, heure, date_reservee, fk_salle, fk_etudiant, fk_creneau) VALUES('$today', '$heure','$date_reservee', '$salle', '$ine', '$creneau2')";
        $pdo->exec($requet);
        echo "success2";
        }
        if (isset($_POST['creneau3'])){
            $creneau3 = $_POST['creneau3'];
            $requet="INSERT INTO reservation(date_reservation, heure, date_reservee, fk_salle, fk_etudiant, fk_creneau) VALUES('$today', '$heure','$date_reservee', '$salle', '$ine', '$creneau3')";
        $pdo->exec($requet);
        echo "success3";
        }
        if (isset($_POST['creneau4'])){
            $creneau4 = $_POST['creneau4'];
            $requet="INSERT INTO reservation(date_reservation, heure, date_reservee, fk_salle, fk_etudiant, fk_creneau) VALUES('$today', '$heure','$date_reservee', '$salle', '$ine', '$creneau4')";
        $pdo->exec($requet);
        echo "success4";
        }
        if (isset($_POST['creneau5'])){
            $creneau5 = $_POST['creneau5'];
            $requet="INSERT INTO reservation(date_reservation, heure, date_reservee, fk_salle, fk_etudiant, fk_creneau) VALUES('$today', '$heure','$date_reservee', '$salle', '$ine', '$creneau5')";
        $pdo->exec($requet);
        echo "success5";
        }

    }
    else
    {
        ECHO "ERREUR";	
    }

?>