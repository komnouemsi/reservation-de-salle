<?php
$host = "localhost"; /* Host name */
$user = "postgres"; /* User */
$password = "root"; /* Password */
$dbname = "bd_reservation"; /* Database name */
$port="5432";

try{
    $con="pgsql:host=" . $host. ";port=" .$port. ";dbname=" .$dbname. ";user=" .$user. ";password=" .$password;

    $pdo = new PDO($con,$user,$password);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_OBJ);
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES,false);
    $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
}

catch (PDOException $e){
    echo 'connection failed' .$e->getMessage();
}


 $sql= "select * from reservation";
 $result = $pdo->exec($sql);

 pg_num_rows($result);
while ($row = pg_fetch_array($result)){
$id=$row['id_reservation'];
$fin= $row['date_reservee'];
 statut($fin, $id);
}
 

function statut($date_de_fin, $id){
    $r1="En cours";
    $r2="Terminé";

    if (date('Y-m-d') <= date('Y-m-d',strtotime($date_de_fin))){
      $sql= "UPDATE reservation SET statut_reservation='$r1' where id_reservation=".$id;
    }
    else {
        $sql= "UPDATE reservation SET statut_reservation='$r2' where id_reservation=".$id;
    }
    $pdo->exec($sql);
  }
?>