<?php

//Cette fonction génère, sauvegarde et retourne un token
//Vous pouvez lui passer en paramètre optionnel un nom pour différencier les formulaires
function generer_token($nom = '')
{
	session_start();
	$token = uniqid(rand(), true);
	$_SESSION[$nom.'_token'] = $token;
	$_SESSION[$nom.'_token_time'] = time();
	return $token;
}

$token = generer_token('enregistrement');
//Ensuite, le formulaire normal, pensez au champ caché. ;)
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Formulaire de réservation</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="../source_checkbox/jquery-labelauty.js"></script>
		<link rel="stylesheet" type="text/css" href="../source_checkbox/jquery-labelauty.css">

		<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="../source/css/bootstrap.min.css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

	<!-- feuille de style -->
	<link type="text/css" rel="stylesheet" href="../source/css/style_formulaire.css" />
	<link type="text/css" rel="stylesheet" href="../source_nav_bar/CSS.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

	</head>
	<body>
<div id="booking" class="section">
	<div id="wrapper">
		<!-- Sidebar -->
		<div id="sidebar-wrapper">
		  <ul class="sidebar-nav">
			<li><a href="#">Mes reservations</a></li>
			<li><a href="#">Déconnexion</a></li>
		  </ul>
		</div>


	<div class="section-center" id="page-content-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-10" style="margin-top: 100px;">
					<button class="btn" id="menu-toggle"><span class="glyphicon glyphicon-menu-hamburger"></span></button>
				</div>
				<div class="booking-form">
					<div class="form-header">
						<h1>Faites votre réservation</h1>
					</div>
					<form enctype="multipart/form-data" action="traitement.php" method="post">
						<div class="form-group" style="visibility: hidden">
							<input class="form-control" type="text" name="ine" value="1234567890A">
							<span class="form-label">Numéro INE</span>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<input class="form-control" type="date" name="date_reservee" required >
									<span class="form-label">Quand la réservation</span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<select class="form-control" name="salle" required>
										<option value="" selected hidden>Sélectionner une salle</option>
										<option value="103">103</option>
										<option value="104">104</option>
										<option value="105">105</option>
										<option value="106">106</option>
										<option value="107">107</option>
										<option value="108">108</option>
										<option value="109">109</option>
										<option value="204">204</option>
										<option value="205">205</option>
									</select>
									<span class="select-arrow"></span>
									<span class="form-label">Salles</span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-check">
										<label class="form-check-label">
											<input type="checkbox" class="agree" name="creneau1" value="8" data-labelauty="08h30 - 10h00 |08h30 - 10h00" disabled>
										  </label>
									<span class="select-arrow"></span>
									<span class="form-label">créneau1</span>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-check">
										<label class="form-check-label">
											<input type="checkbox" class="agree" name="creneau2" value="9" data-labelauty="10h30 - 12h00 |10h30 - 12h00">
										  </label>
									<span class="select-arrow"></span>
									<span class="form-label">créneau2</span>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-check">
										<label class="form-check-label">
											<input type="checkbox" class="agree" name="creneau3" value="10" data-labelauty="13h30 - 15h00 |13h30 - 15h00">
										  </label>
									<span class="select-arrow"></span>
									<span class="form-label">créneau3</span>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-check">
										<label class="form-check-label">
											<input type="checkbox" class="agree" name="creneau4" value="11" data-labelauty="15h15 - 16h45 |15h15 - 16h45">
										  </label>
									<span class="select-arrow"></span>
									<span class="form-label">créneau4</span>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-check">
										<label class="form-check-label">
											<input type="checkbox" class="agree" name="creneau5" value="12" data-labelauty="17h00 - 18h30 |17h00 - 18h30">
										  </label>
									<span class="select-arrow"></span>
									<span class="form-label">créneau5</span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<input class="form-control" type="email" name="email" placeholder="Entrer Email">
									<span class="form-label">Email</span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input class="form-control" type="hidden" id="token" name="token" value="<?php
										//Le champ caché a pour valeur le jeton
										echo $token;
											?>">
								</div>
							</div>
						</div>
						<div class="form-btn">
							<button class="submit-btn">Valider</button>
						</div>
					</form>
				</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>

<script src="js/jquery.min.js"></script>
<script>
	$('.form-control').each(function () {
		floatedLabel($(this));
	});

	$('.form-control').on('input', function () {
		floatedLabel($(this));
	});

	function floatedLabel(input) {
		var $field = input.closest('.form-group');
		if (input.val()) {
			$field.addClass('input-not-empty');
		} else {
			$field.removeClass('input-not-empty');
		}
	}
</script>

<!--fonction checkbox-->
<script>
	$(document).ready(function(){
		$(":checkbox").labelauty();
	});
</script>

<!--fonction agree-->
<script>
    $(document).ready(function(){
        $('form div button[class="submit-btn"]').prop("disabled", true);
        $(".agree").click(function(){
            if($(this).prop("checked") == true){
                $('form div  button[class="submit-btn"]').prop("disabled", false);
            }
            else if($(this).prop("checked") == false){
                $('form div button[class="submit-btn"]').prop("disabled", true);
            }
        });
    });
</script> 
<!--fonction navbar-->
<script>
	$(document).ready(function(){
  $("#menu-toggle").click(function(e){
    e.preventDefault();
    $("#wrapper").toggleClass("menuDisplayed");
  });
});

</script>
	</body>
</html>
